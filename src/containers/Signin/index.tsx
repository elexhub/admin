import React from "react";
import "@ionic/react/css/core.css";
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "../../styles/index.css";
import { Container, Form, Row, Col } from "react-bootstrap";
import Layout from "../../components/Layout";
import { IonButton } from "@ionic/react";
import Input from "../../components/UI/input";

/**
  @author abdfnx
  @function Signin
**/

const Signin = (props: any) => {
  return (
    <Layout>
      <Container>
        <Row style={{ marginTop: "80px" }}>
          <Col md={{ span: 6, offset: 3 }}>
            <Form>
              <Input
                label="Email"
                placeholder="Your email"
                value=""
                type="email"
                onChange={() => {}}
              />

              <Input
                label="Password"
                placeholder="Password"
                value=""
                type="Password"
                onChange={() => {}}
              />
              <Form.Group controlId="formBasicCheckbox">
                {/* <IonItem>
                  <IonLabel>Remember me</IonLabel>
                  <IonCheckbox
                    color="primary"
                    slot="start"
                  ></IonCheckbox>
                </IonItem> */}
              </Form.Group>
              <IonButton type="submit" style={{ fontFamily: "Poppins" }}>Submit</IonButton>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Signin;
