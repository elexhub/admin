import React from "react";
import "@ionic/react/css/core.css";
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "../../styles/index.css";
import Layout from "../../components/Layout";
import { Jumbotron } from "react-bootstrap";
import { elexhub_url } from "../../tools/urls";
import OurSites from "../../components/UI/our_sites";

/**
  @author abdfnx
  @function Home
**/

const Home = (props) => {
  return (
    <div>
      <Layout>
        <Jumbotron style={{ margin: "1rem", background: "#fff" }}>
          <h1>Welcome to Admin Dashboard</h1>
          <p>
            If you're an <strong>admin</strong> in{" "}
            <a href={elexhub_url} style={{ textDecoration: "none" }}>
              elexhub
            </a>
            , you should{" "}
            <a style={{ textDecoration: "none" }} href="/signin">
              signin
            </a>{" "}
            & modify... <br /> or you can be an <strong>admin</strong> by{" "}
            <a href="/signup" style={{ textDecoration: "none" }}>
              signup
            </a>
          </p>
        </Jumbotron>
        <OurSites />
      </Layout>
    </div>
  );
};

export default Home;
