const elexhub_url = "https://elexhub.web.app";
const bb_admin_repo = "https://bitbucket.org/Elexhub/admin/src/master/";

export { elexhub_url, bb_admin_repo };
