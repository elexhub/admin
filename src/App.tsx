import React from "react";
import { IonApp, IonRouterOutlet } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "./styles/index.css";
import { Route, Switch } from "react-router-dom";

// routes
import Home from "./containers/Home";
import Signin from "./containers/Signin";
import Signup from "./containers/Signup";

const App: React.FC = () => (
  <IonApp>
    <div>
      <IonReactRouter>
        <IonRouterOutlet>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/signin" component={Signin} />
            <Route path="/signup" component={Signup} />
          </Switch>
        </IonRouterOutlet>
      </IonReactRouter>
    </div>
  </IonApp>
);

export default App;
