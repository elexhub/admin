import React from "react";
import "@ionic/react/css/core.css";
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "../../../styles/index.css";
import { Form } from "react-bootstrap";

/**
  @author abdfnx
  @function Input
**/

const Input = (props: any) => {
  return (
    <Form.Group>
      <Form.Label style={{ fontFamily: "PS", fontWeight: 'bold' }}>{props.label}</Form.Label>
      <Form.Control
        type={props.type}
        placeholder={props.placeholder} style={{ fontFamily: "Poppins" }}
        value={props.value}
        onChange={props.onChange}
      />
      <Form.Text className="text-muted">{props.errMsg}</Form.Text>
    </Form.Group>
  );
};

export default Input;
